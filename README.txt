
INTRODUCTION
============

This module provides Writeup filter integration for Drupal input formats.

 * Writeup has a superficial similarity with Markdown, and is designed to co-
   exist with HTML. It is also meant to be as human-readable as possible.
 * Writeup is vastly more sophisticated than Markdown or other similar markup
   styles, with variables, functions and a full-blown text processing language.
 * Most markup characters are programmable so that the syntax of Markdown,
   Textile or other systems can be emulated
 * For more information, see see the writeup.org ( http://writeup.org/ )
   website.

IMPLEMENTATION
==============

 * In this initial implementation, only a small subset of writeup has been
   implemented
 * A more complete solution will be released shortly

CONFIGURATION
=============

1. Install the module
2. Create an input format using /admin/settings/filters
3. Apply the format to input text, as required

FUTURE DEVELOPMENT
==================

 * Optional integration with full writeup engine found at
   http://writeup.sourceforge.net

SUPPORT
=======

If you experience a problem with Writeup Filter or have a problem, file a
request or issue on the Writeup Filter queue at http://drupal.org/project/
issues/writeup. DO NOT POST IN THE FORUMS. Posting in the issue queues is a
direct line of communication with the module authors.

No guarantee is provided with this software, no matter how critical your
information, module authors are not responsible for damage caused by this
software or obligated in any way to correct problems you may experience.


SPONSORS
========

The Writeup Filter module is sponsored by Crazy Korean Cooking
( http://crazykoreancooking.com ).

Licensed under the GPL 2.0.
http://www.gnu.org/licenses/gpl-2.0.txt

